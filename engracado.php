<!DOCTYPE html>
<html>
<head>
	<title>Adote Seu Melhor Amigo</title>
	<meta charset='utf-8'>
        <meta name='viewport' content='width=device-width, initial-scale=1'>
        <link rel='stylesheet' href='css/uikit.css'>
   		<link rel='stylesheet' href='css/style.css'>
</head>
<body>
	<?php
	echo "
	<div class='uk-child-width-expand@s uk-text-left' uk-grid>
    <div>
        <div class='uk-card uk-card-default uk-card-body'>
		<form action='recebe.php' method='post' enctype='multipart/form-data'>
				
				<legend class='uk-legend' style='color:black'> Adicionar Imagens</legend>
        <div class='uk-margin' uk-margin>
        <div uk-form-custom='target: true'>
            <input type='file' name='foto' id='foto'>
            <input class='uk-input uk-form-width-medium' type='text' placeholder='Select file' disabled>
        </div>
        <button class='uk-button uk-button-default'>Submit</button>
    </div>
				
				<legend class='uk-legend' style='color:black'>Adicionar legenda</legend>
				<div class='uk-margin'>
					<div class='uk-inline'>
					<span class='uk-form-icon' uk-icon='icon: home' style='color:#00BFFF'></span>
					<input class='uk-input uk-form-width-large' type='text' placeholder='legenda' name='legenda' id='legenda' required>
					</div>
				</div>
				
				
			
      
    
            <button class='uk-button uk-button-default'  onclick=\"UIkit.notification({message: '<span uk-icon=\'icon: check\'></span> Modificações realizadas com sucesso'})\">Modificar</button>
    
			</form>
			</div>
		</div>
	</div>";
	?>
</body>
<script src='js/uikit.min.js'></script>
	<script src='js/uikit-icons.min.js'></script>

</html>
