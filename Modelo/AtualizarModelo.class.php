<?php
	class AtualizarModelo{
	private $id;
	private $titulo1;
	private $titulo2;
	private $textoUm;
	private $textoDois;
	private $imagem;
	private $tipo;
	private $video;
	private $tipoVideo;
	private $legenda;
	public function getId(){
		return $this->id;
	}
	public function setId($id){
		$this->id = ($id != NULL) ? $id : NULL;
	}
	public function getTitulo1(){
		return $this->titulo1;
	}
	public function setTitulo1($titulo1){
		$this->titulo1 = ($titulo1 != NULL) ? $titulo1 : NULL;
	}
	public function getTitulo2(){
		return $this->titulo2;
	}
	public function setTitulo2($titulo2){
		$this->titulo2 = ($titulo2 != NULL) ? $titulo2 : NULL;
	}
	public function getTextoUm(){
		return $this->textoUm;
	}
	public function setTextoUm($textoUm){
		$this->textoUm = ($textoUm != NULL) ? $textoUm : NULL;
	}
	public function getTextoDois(){
		return $this->textoDois;
	}
	public function setTextoDois($textoDois){
		$this->textoDois = ($textoDois != NULL) ? $textoDois : NULL;
	}
    public function getImagem(){
        return $this->imagem;
    }
    public function setImagem($imagem){
        $this->imagem = ($imagem != NULL) ? $imagem : NULL;
	}
	public function getTipo(){
		return $this->tipo;
	}
	public function setTipo($tipo){
		$this->tipo = ($tipo != NULL) ? $tipo : NULL;
	}
	public function getVideo(){
		return $this->video;
	}
	public function setVideo($video){
		$this->video = ($video != NULL) ? $video : NULL;
	}
	public function getTipoVideo(){
		return $this->tipoVideo;
	}
	public function setTipoVideo($tipoVideo){
		$this->tipoVideo = ($tipoVideo != NULL) ? $tipoVideo : NULL; 
	}
	public function getLegenda(){
		return $this->legenda;
	}
	public function setLegenda($legenda){
		$this->legenda = ($legenda != NULL) ? $legenda : NULL;
	}
}	
?>
