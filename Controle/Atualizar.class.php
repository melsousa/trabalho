<?php
require_once("Conexao.class.php");
require_once("Modelo/AtualizarModelo.class.php");
final class Atualizar{
	 public function selecionarId($id){
        $conexao = new Conexao("Controle/xuxu.ini");
        $comando = $conexao->getConexao()->prepare("SELECT * FROM atualizar WHERE id=:id");
        $comando->bindParam("id", $id);
        $comando->execute();
        $consulta = $comando->fetch();
        $atualizar = new AtualizarModelo();
        $atualizar->setId($consulta->id);
        $atualizar->setTitulo1($consulta->titulo1);
        $atualizar->setTitulo2($consulta->titulo2);
        $atualizar->setTextoUm($consulta->textoUm);
        $atualizar->setTextoDois($consulta->textoDois);
        $atualizar->setImagem($consulta->imagem);
        $atualizar->setTipo($consulta->tipo);
        $atualizar->setVideo($consulta->video);
        $atualizar->setTipoVideo($consulta->tipoVideo);
        $conexao->__destruct();
        return $atualizar;
    }
    public function selecionarTodos(){
        $conexao = new Conexao("Controle/xuxu.ini");
        $comando = $conexao->getConexao()->prepare("SELECT * FROM atualizar ORDER BY id DESC");
        $comando->execute();
        $resultado = $comando->fetchAll();
        $lista = [];
        foreach($resultado as $item){
            $atualizar = new AtualizarModelo();
            $atualizar->setId($item->id);
            $atualizar->setTitulo1($item->titulo1);
            $atualizar->setTitulo2($item->titulo2);
            $atualizar->setTextoUm($item->textoUm);
            $atualizar->setTextoDois($item->textoDois);
            $atualizar->setTipo($item->tipo);
            $atualizar->setTipoVideo($item->tipoVideo);
	    $atualizar->setLegenda($item->legenda);
            array_push($lista, $atualizar);
        }
        $conexao->__destruct();
        return $lista;
    }
    
        public function adicionarImagens($imagem){
        $conexao = new Conexao("Controle/xuxu.ini");
        $imagemTipo = explode("/", $imagem->getTipo());
        $imagemTipo = $imagemTipo[1];
        $imagemBin = file_get_contents($imagem->getImagem());
        $videoTipo = explode("/", $imagem->getTipoVideo());
        $videoTipo = $videoTipo[1];
        $videoBin = file_get_contents($imagem->getVideo());
        $sql= "INSERT INTO atualizar(titulo1,titulo2,textoUm,textoDois, imagem, tipo, legenda, video, tipoVideo) VALUES(:ti,:ti2,:tex,:tex2,:img,:tp,:lg,:vd,:tv)";
        $comando = $conexao->getConexao()->prepare($sql);
        $comando->bindValue("ti", $imagem->getTitulo1());
        $comando->bindValue("ti2", $imagem->getTitulo2());
        $comando->bindValue("tex", $imagem->getTextoUm());
        $comando->bindValue("tex2", $imagem->getTextoDois());
        $comando->bindValue("img", $imagemBin);
        $comando->bindValue("tp", $imagemTipo);
        $comando->bindValue("lg", $imagem->getLegenda());
        $comando->bindValue("vd", $videoBin);
        $comando->bindValue("tv", $videoTipo);
        if($comando->execute()){
            $conexao->__destruct();
            return true;
        }else{
            $conexao->__destruct();
        }
    }
    
    public function atualizaTitulos($atualizar){
        $conexao = new Conexao("Controle/xuxu.ini");
        $imagemTipo = explode("/", $atualizar->getTipo());
        $imagemTipo = $imagemTipo[1];
        $imagemBin = file_get_contents($atualizar->getImagem());
        $videoTipo = explode("/", $atualizar->getTipoVideo());
        $videoTipo = $videoTipo[1];
        $videoBin = file_get_contents($atualizar->getVideo());
        
        $sql ="UPDATE atualizar SET titulo1=:tit, titulo2=:tit2, textoUm=:tex,textoDois=:tex2,imagem=:img,tipo=:tp,video=:vd,tipoVideo=:tv WHERE id=:id;";
        $comando = $conexao->getConexao()->prepare($sql);
        $comando->bindValue("id", $atualizar->getId());
        $comando->bindValue("tit", $atualizar->getTitulo1());
        $comando->bindValue("tit2", $atualizar->getTitulo2());
        $comando->bindValue("tex", $atualizar->getTextoUm());
        $comando->bindValue("tex2", $atualizar->getTextoDois());
        $comando->bindValue("img", $imagemBin);
        $comando->bindValue("tp", $imagemTipo);
        $comando->bindValue("vd", $videoBin);
        $comando->bindValue("tv", $videoTipo);
        $comando->execute();
        $conexao->__destruct();
        return true;
    }
    public function remover($id){
        $conexao = new Conexao("Controle/xuxu.ini");
        $comando = $conexao->getConexao()->prepare("DELETE FROM atualizar WHERE id=:id");
        $comando->bindValue("id", $id);
        $comando->execute();
        $conexao->__destruct();
        //<a href='deletar.php?id={$livro->getId()}'>Apagar</a>
    }
    
}
?>
