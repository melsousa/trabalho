<?php
require_once("Conexao.class.php");
require_once("Modelo/UsuarioModelo.class.php");
final class Usuario{
	public function adicionar($adicionar){
		$conexao = new Conexao("Controle/xuxu.ini");
		$sql = "INSERT INTO usuario(nome,email,endereco,telefone) VALUES(:nm,:em,:ed,:tf)";
		$comando = $conexao->getConexao()->prepare($sql);
		$comando->bindValue("nm", $adicionar->getNome());
		$comando->bindValue("em", $adicionar->getEmail());
		$comando->bindValue("ed", $adicionar->getEndereco());
		$comando->bindValue("tf", $adicionar->getTelefone());
		if($comando->execute()){
		    $conexao->__destruct();
		    return true;
		}else{
		    $conexao->__destruct();
		}
	}
	public function selecionar(){
		$conexao = new Conexao("Controle/xuxu.ini");
		$comando = $conexao->getConexao()->prepare("SELECT * FROM usuario;");
        $comando->execute();
        $resultado = $comando->fetchAll();
        $lista = [];
        foreach ($resultado as $item) {
        	$atualizar = new UsuarioModelo();
        	$atualizar->setId($item->id);
        	$atualizar->setNome($item->nome);
        	$atualizar->setEmail($item->email);
        	$atualizar->setEndereco($item->endereco);
        	$atualizar->setTelefone($item->telefone);
        	array_push($lista, $atualizar);
        }
        $conexao->__destruct();
        return $lista;
	}
} 
?>
