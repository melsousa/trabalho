<!DOCTYPE html>
<html>
<head>
	<title></title>
		<meta charset='utf-8'>
        <meta name='viewport' content='width=device-width, initial-scale=1'>
        <link rel='stylesheet' href='css/uikit.css'>
        <link rel='stylesheet' href='css/simplezoom.css'> 
        <link rel='stylesheet' href='css/style.css'>

</head>
<body>
	<?php
		echo "
		
		<form action='trocarSite.php' method='post' enctype='multipart/form-data'>
	        <div class='uk-margin' uk-margin>
	        <div uk-form-custom='target: true'>
	            <legend class='uk-legend'> Adicionar imagem</legend>
	        <br>
	            <input type='file' name='imgParallax' aceppt='image/png , image/jpg, image/jpeg'>
	            <input class='uk-input uk-form-width-medium' type='text' placeholder='Selecione a imagem' disabled><br><br>
	            
			<br>
	        </div>
        <br>
		<div uk-form-custom=\"target: true\">
		<legend class='uk-legend'> Alterar video</legend>
            <input type='file' name='video'>
			<input class='uk-input uk-form-width-medium' type='text' placeholder='Selecione o vídeo' disabled>
		</div>
        
    	</div>
    	<legend class='uk-legend'>Alterar titulo 1</legend>
    		<div class='uk-margin'>
        		<input class='uk-input uk-form-width-medium' type='text' name='titulo' id='titulo' placeholder='Digite aqui'><br><br>
   			 </div>

   			  
    	<legend class='uk-legend'>Alterar titulo 2</legend>
    		<div class='uk-margin'>
        		<input class='uk-input uk-form-width-medium' type='text' name='titulo2' id='titulo2' placeholder='Digite aqui'>
   			 </div>  	
    	<legend class='uk-legend'>Alterar texto 1</legend>
    		 <div class='uk-margin'>
            	<textarea class='uk-textarea' rows='3' placeholder='Digite aqui' name='TextoUm' id='TextoUm'></textarea><br><br>
            </div>
        <legend class='uk-legend'>Alterar texto 2</legend>
    		 <div class='uk-margin'>
            	<textarea class='uk-textarea' rows='3' placeholder='Digite aqui' name='TextoDois' id='TextoDois'></textarea><br><br>
            </div>

    	

            <button class='uk-button uk-button-default'  onclick=\"UIkit.notification({message: '<span uk-icon=\'icon: check\'></span> Modificações realizadas com sucesso'})\">Modificar</button>
        
        </form>
		";
	?>
</body>
<script src='js/simplezoom.js'></script>
<script src='js/uikit.min.js'></script>

</html>
